import React, { Component } from 'react'
import { subscribeToTimer } from '../Modules/subscribeToTimer';
const moment = require('moment');

class About extends Component {

  constructor(props) {
    super(props);
    subscribeToTimer((err, timestamp) => this.setState({ 
      timestamp : moment(timestamp).format('MMMM Do YYYY, h:mm:ss a')
    }));
  }

  state = {
    timestamp: 'no timestamp yet'
  };
  
  render () {
    return (
      <div>
        <div>About View</div>
        <p className="App-intro">
        This is the timer value: {this.state.timestamp}
        </p>
      </div>
    );
  }
}

export default About