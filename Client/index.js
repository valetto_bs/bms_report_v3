import React from 'react';
import ReactDOM from 'react-dom';
import AppComponent from './App';
import './sass/app.scss';

ReactDOM.render(<AppComponent />, document.getElementById('app'));