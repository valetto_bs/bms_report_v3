import React, { Component } from 'react';
import Navbar from './components/Header/Navbar';
import { BrowserRouter } from 'react-router-dom'
import Routes from './Routes'

export default class App extends Component {

    constructor(){
        super();

        this.state = {
            title: 'React Starter',
            description: 'A basic template that consists of the essential elements that are required to start building a React application'
        };
    }

    render() {
        return (
            <BrowserRouter>
            <div>
                <Navbar />
                <main role="main" className="container">
                    <Routes />
                </main>
            </div>
            </BrowserRouter>
        );
    }
}