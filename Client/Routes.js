import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import home from './components/pages/Home'
import about from './components/pages/About'

class Routes extends Component {
  render () {
    return (
        <Switch>
          <Route exact path='/' component={home} />
          <Route exact path='/about' component={about} />
        </Switch>
    )
  }
}

export default Routes