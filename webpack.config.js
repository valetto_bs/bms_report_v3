const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const Dotenv = require('dotenv-webpack');
const CleanWebPackPlugin = require('clean-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');

const config = {
	mode: "development",
    entry: './Client/index.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'public')
    },
	module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.s?css$/,
                use: [ { loader: MiniCssExtractPlugin.loader }, 'css-loader', 'sass-loader' ],
                exclude: /node_modules/
            },
            {
                test: /\.svg$/,
                loader: 'url-loader',
                exclude: /node_modules/
            }
        ]
	},
    devtool: 'cheap-module-eval-source-map',
    devServer: {
      historyApiFallback: true,
    },
    plugins: [
    	new Dotenv(),
        new MiniCssExtractPlugin({
		  filename: 'bundle.css',
		  chunkFilename: '[id].css',
        }),
        new CleanWebPackPlugin(),
        new HtmlWebPackPlugin({
            template: './Client/index.html',
            favicon: './Client/favicon.ico',
            inject: false
        })
    ]
};

module.exports = config;