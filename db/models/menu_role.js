/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('menu_role', {
		menu_id: {
			type: DataTypes.INTEGER(11).UNSIGNED,
			allowNull: false,
			references: {
				model: 'menus',
				key: 'id'
			}
		},
		role_id: {
			type: DataTypes.INTEGER(11).UNSIGNED,
			allowNull: false,
			references: {
				model: 'roles',
				key: 'id'
			}
		}
	}, {
		tableName: 'menu_role',
		timestamps: false
	});
};
