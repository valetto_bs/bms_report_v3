/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('menus', {
		id: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		label: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		title: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		slug: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		robot: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		style: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		target: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		auth: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		lang: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		parent: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			defaultValue: '0'
		},
		sort: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			defaultValue: '0'
		},
		type: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		created_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
		},
		updated_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
		}
	}, {
		tableName: 'menus',
		timestamps: false
	});
};
