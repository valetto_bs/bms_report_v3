/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('rate', {
		id: {
			type: DataTypes.INTEGER(10).UNSIGNED,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		date: {
			type: DataTypes.DATEONLY,
			allowNull: false,
			unique: true
		},
		base_currency: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		sgd_rate: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		rates: {
			type: DataTypes.TEXT,
			allowNull: false
		},
		timestamp: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		created_at: {
			type: DataTypes.DATE,
			allowNull: true
		},
		updated_at: {
			type: DataTypes.DATE,
			allowNull: true
		}
	}, {
		tableName: 'rate'
	});
};
