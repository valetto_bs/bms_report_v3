const Sequelize = require('sequelize')

// DB Configs
const config = require('../db/config/database.json');
const keywoBlogConfig = config['keywo_blog'];
const microConfig = config['micro'];

// Connections
const keywoBlogDb = new Sequelize(keywoBlogConfig.database, keywoBlogConfig.username, keywoBlogConfig.password, keywoBlogConfig);
const microDb = new Sequelize(microConfig.database, microConfig.username, microConfig.password, microConfig);

// Models
const UserModel = require('../db/models/users')
const OauthClientsModel = require('../db/models/oauth_clients')
const MenusModel = require('../db/models/menus')

// Mapping
const User = UserModel(keywoBlogDb, Sequelize)
const OauthClient = OauthClientsModel(keywoBlogDb, Sequelize)
const Menu = MenusModel(microDb, Sequelize)

module.exports = {
  User,
  OauthClient,
  Menu
}