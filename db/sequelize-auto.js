var SequelizeAuto = require('sequelize-auto')

var auto = new SequelizeAuto('exchange', 'microuser', 'microuser@123',{
  host: '192.168.2.27',
  port:'3306',
  additional: {
      timestamps: false
  },
  tables: ['menus', 'menu_role']
});

auto.run(function (err) {
  if (err) throw err;

  console.log(auto.tables); // table list
  console.log(auto.foreignKeys); // foreign key list
});