// package references
const app = require('express')();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const PORT = process.env.PORT || 8000;

// dotenv
const dotenv = require('dotenv');
dotenv.config();

// sequelize
const { User, OauthClient, Menu } = require('../db/sequelize');
User.findAll().then(users => {
   console.log("All users:", JSON.stringify(users, null, 4));
 });

OauthClient.findAll().then(clients => {
   console.log("All clients:", JSON.stringify(clients, null, 4));
});

Menu.findAll().then(menus => {
   console.log("All menus:", JSON.stringify(menus, null, 4));
});

// app references
const mainRouter = require('./routers/router');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('combined'));
app.use(cors());
app.use('/api', mainRouter(PORT));

app.get('*', (req, res) => {
   res.json({"msg":"invalid request"});
});

const server = require('http').createServer(app);
const io = require('socket.io')(server);

// io.on('connection', function(socket){
//    console.log('an user connected');
// });

io.on('connection', (client) => {
   console.log('an user connected');
   client.on('subscribeToTimer', (interval) => {
     console.log('client is subscribing to timer with interval ', interval);
     setInterval(() => {
       client.emit('timer', new Date());
     }, interval);
   });
 });

// start server
server.listen(PORT, () => {
   console.log(`Listening on port ${PORT} ...`);
});