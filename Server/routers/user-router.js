// package references
const express = require('express');

// build router
const router = () => {
    const routes = express.Router();
    routes
        .get('*', (req, res) => {
            console.log('inside user');
            res.json({"msg":"user data"});
        })

    return routes;
};

module.exports = router;