// package references
const express = require('express');

// build router
const router = () => {
    const routes = express.Router();
    routes
        .get('*', (req, res) => {
            console.log('inside bot');
            res.json({"msg":"bot data"});
        })

    return routes;
};

module.exports = router;