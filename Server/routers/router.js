// package references
const express = require('express');

// app references
const botRouter = require('./bot-router');
const userRouter = require('./user-router');

// configure server
const server = express();

// build router
const mainRouter = (PORT) => {
	
    server.use('/bot', botRouter(PORT));
    server.use('/user', userRouter(PORT));
    return server;

};

module.exports = mainRouter;